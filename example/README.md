# example

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


## We Are Mobile First

This project represents a staring place for projects to adhere to our ways of working.

Please read the following posts:

- [Atomic design](https://www.wearemobilefirst.com/blog/atomic-design)
- [Source control model](https://www.wearemobilefirst.com/blog/how-to-use-the-full-power-of-source-control-and-agile-release-procedures)
- [General coding practice](https://www.wearemobilefirst.com/blog/healthy-coding-practices)
